function updateUI() {
	var winWidth = $(window).width();
	var winHeight = $(window).height();
	$(".main").height(winHeight-73);
	$(".mainarea").width(winWidth-110);
	$(".leftToolbar").height(winHeight-73);
	$(".rightColorBar").height(winHeight-73);
	$(".mainarea").height(winHeight-88);
	$(".navbar").css("width",winWidth-11);
	$("#bottom").css("width",winWidth-110);
	$(".leftToolbarContainer").height(winHeight-84);
	$(".rightbarContainer").height(winHeight-84);
};


function ColorPicker(id, x, y) {
	var ctx = document.getElementById(id).getContext("2d");
	var imageData = ctx.getImageData(x,y,1,1);
	var pixel = imageData.data;
	console.log(pixel);
	var pixelColor = "rgba("+pixel[0]+", "+pixel[1]+", "+pixel[2]+", "+pixel[3]+")";
	return pixelColor;
};
//底部状态栏显示：画布尺寸，当前点的坐标，放大倍数
function StatusSet(w,h,x,y,scale) {
	x = Math.round(x);
	y = Math.round(y);
	var string=w.toString()+"*"+h.toString()+" ("+x.toString()+","+y.toString()+")  当前缩放比例:"+scale.toString()+"%";
	$("#bottom").html(string);
}
