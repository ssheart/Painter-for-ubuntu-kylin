// JavaScript Document
/*
	定义各类绘图方法，封装成绘图对象。
	在Draw方法内部设置各种标志：
		1：绘图状态，在mousedown的时候查看Draw方法是否正在工作，working。
		2：绘图对象，可以记录下图形的轨迹。
		3：绘图色板、线条、字体、类型等标志,由参数Tool传递。
		4：绘图状态的释放。
	当曲线需要多次动作绘制时，第一次总是出来一条直线。
*/
function myDraw(canvas) {
	var working = false;
	var mouse = 0;
	var pointList = new Array();
	var ctx = canvas.getContext();
	//绘图画笔参数
	var strokeStyle;
	var fillStyle;
	var lineWidth;
	var font;
	var type;
	var bezierStep = 0;
	var step = 0;
	var curScale = 1;		//放大系数
	pointList.startX = 0;
	pointList.startY = 0;
	pointList.endX = 0;
	pointList.endY = 0;
	pointList.cpX1 = 0;
	pointList.cpY1 = 0;
	pointList.cpX2 = 0;
	pointList.cpY2 = 0;
	this.Rect = new Array();		//获取当前所在的矩形区域
	 this.Rect.left = 0;
	 this.Rect.top =  this.Rect.right =  this.Rect.bottom = 0;
	
	this.beginDraw = function(tool) {
		working = true;
		strokeStyle = tool.getStrokeStyle();
		fillStyle = tool.getFillStyle();
		lineWidth = tool.getLineWidth();
		font = tool.getFontStyle();
		type = tool.getName();
		step = 0;
		bezierStep = 0;
	}
	this.endDraw = function(drawed) {
		working = false;
		step = 0;
		bezierStep = 0;
		if (drawed == true) {
			canvas.historyPush();
		}
	}
	this.checkState = function() {
		return working;
	}
	this.setType = function(t) {
		type = t;
	}
	this.getType = function() {
		return type;
	}
	this.setCurScale = function(s) {
		curScale = s;
		console.log(curScale);
	}
	this.getCurScale = function() {
		return curScale;
	}
	this.getBesizerStep = function() {
		return besizerStep;
	}
	this.getStep = function() {
		return step;
	}
	this.setMouse = function(m) {
		mouse = m;
	}
	this.getMouse = function() {
		return mouse;
	}
	this.OneStep = function() {
		if (type == "bezier"){
			step++;
			besizerStep++;
			console.log("Bezier Step ",step);
		} else if (type=="pencilButton" || type=="brushButton" || type=="textButton"){
			step+=10;
		} else {
			step += 2;
		}
	}
	this.keepState = function() {
		step = 2;
	}
	this.getRect = function() {
		return this.rect;
	}
	this.draw = function(x1,y1,x2,y2,cpx1,cpy1) {
		pointList.startX = x1;
		pointList.startY = y1;
		pointList.endX = x2;
		pointList.endY = y2;
		pointList.cpX1 = cpx1;
		pointList.cpY1 = cpy1;
		ctx.strokeStyle = (mouse==0?strokeStyle:fillStyle);
		ctx.fillStyle = (mouse==0?fillStyle:strokeStyle);
		ctx.lineWidth = lineWidth;
		ctx.font = font;
		
		//绘制图形
		switch(type) {
			case "Line": 		canvas.clearCanvas();
								canvas.reDraw();
								this.line(x1,y1,x2,y2);
								this.Rect.left = Math.min(x1,x2);
								 this.Rect.top = Math.min(y1,y2);
								 this.Rect.right = Math.max(x1,x2);
								 this.Rect.bottom = Math.max(y1,y2);
								console.log("moving");
								break;
			case "brushButton":		this.brush(x1,y1,x2,y2);
								break;
			case "Rectangle":	canvas.clearCanvas();
								canvas.reDraw();
								this.rect(x1,y1,x2,y2);
								this.Rect.left = x1;
								this.rect.top = y1;
								this.Rect.right = x2;
								this.Rect.bottom = y2;
								break;
			case "Ellipse":		canvas.clearCanvas();
								canvas.reDraw();
								this.ellipse(x1,y1,x2,y2);
								 this.Rect.left = Math.min(x1,x2);
								 this.Rect.right = Math.max(x1,x2);
								 this.Rect.top = y1 - Math.abs(y1-y2);
								 this.Rect.bottom = y1 + Math.abs(y1-y2);
								break;
			case "RoundRect":	canvas.clearCanvas();
								canvas.reDraw();
								this.roundRect(x1,y1,x2,y2);
								 this.Rect.left = Math.min(x1,x2);
								 this.Rect.top = Math.min(y1,y2);
								 this.Rect.right = Math.max(x1,x2);
								 this.Rect.bottom = Math.max(y1,y2);
								break;
			case "Triangle":	canvas.clearCanvas();
								canvas.reDraw();
								this.triangle(x1,y1,x2,y2);
								this.roundRect(x1,y1,x2,y2);
								 this.Rect.left = Math.min(x1,x2);
								 this.Rect.top = Math.min(y1,y2);
								 this.Rect.right = Math.max(x1,x2);
								 this.Rect.bottom = Math.max(y1,y2);
								break;
			case "Wujiaoxing": 	canvas.clearCanvas();
								canvas.reDraw();
								this.star(x1,y1,x2,y2);
								break;
			case "Bezier":		canvas.clearCanvas();
								canvas.reDraw();
								if (bezierStep == 0) {
									this.line(x1,y1,x2,y2);
									 this.Rect.left = Math.min(x1,x2);
									 this.Rect.top = Math.min(y1,y2);
									 this.Rect.right = Math.max(x1,x2);
									 this.Rect.bottom = Math.max(y1,y2);
								}
								if (bezierStep == 1) {
									this.besizer(x1,y1,x2,y2,cpx1,cpy1);
									 this.Rect.left = Math.min( this.Rect.left,cpx1);
									 this.Rect.top = Math.min( this.Rect.top,cpy1);
									 this.Rect.right = Math.max( this.Rect.right,cpx1);
									 this.Rect.bottom = Math.max( this.Rect.bottom,cpy2);
								}
								break;
			case "pencilButton":
								this.line(x1,y1,x2,y2);
								Rect.left = Math.min(Rect.left,x1);
								Rect.top = Math.min(Rect.top,y1);
								Rect.right = Math.max(Rect.right,x1);
								Rect.bottom = Math.max(Rect.bottom,y1);
								
								break;
			case "eraserButton":
								this.eraser(x1,y1,"",10);
								break;
			case "textButton":	$("#textField").css("margin-left",x1);
								$("#textField").css("margin-top",y1);
								$("#textField").css("width",x2-x1);
								$("#textField").css("height",y2-y1);
								$("#textField").css("display","block");
								console.log("text show");
			default: 			break;
		}
	}
	this.adjust = function(offsetX,offsetY) {
		var x1 = pointList.startX + offsetX;
		var y1 = pointList.startY + offsetY;
		var x2 = pointList.endX + offsetX;
		var y2 = pointList.endY + offsetY;
		var cpx1 = pointList.cpX1 + offsetX;
		var cpy1 = pointList.cpX2 + offsetY;
		switch(type) {
			case "Line": 		canvas.clearCanvas();
								canvas.reDraw();
								this.line(x1,y1,x2,y2);
								break;
			case "Rectangle":	canvas.clearCanvas();
								canvas.reDraw();
								this.rect(x1,y1,x2,y2);
								break;
			case "Ellipse":		canvas.clearCanvas();
								canvas.reDraw();
								this.ellipse(x1,y1,x2,y2);
								break;
			case "RoundRect":	canvas.clearCanvas();
								canvas.reDraw();
								this.roundRect(x1,y1,x2,y2);
								break;
			case "Triangle":	canvas.clearCanvas();
								canvas.reDraw();
								this.triangle(x1,y1,x2,y2);
								break;
			case "Bezier":		console.log("未实现");
								break;
			case "Wujiaoxing":	this.star(x1,y1,x2,y2);
			default:			break;
		}
	}
	 
	//原来写的方法
	//以下方法需要每次保存画布状态并重绘前一个界面，以保证动态效果
	this.brush = function(startX,startY,endX,endY) {
		ctx.fillStyle = strokeStyle;
		ctx.beginPath();
		ctx.arc(endX,endY,3,Math.PI*2,false);
		ctx.fill();
		ctx.closePath();
	}
	this.line = function(startX,startY,endX,endY) {
		ctx.beginPath();
		ctx.moveTo(startX,startY);
		ctx.lineTo(endX,endY);
		ctx.stroke();
		ctx.closePath();
	}
	this.besizer = function(startX,startY,endX,endY,cpX,cpY) {
		ctx.beginPath();
		ctx.moveTo(startX,startY);
		ctx.quadraticCurveTo(cpX,cpY,endX,endY);
		ctx.stroke();
		ctx.closePath();
	}
	this.rect = function(startX,startY,endX,endY) {
		var x = startX;
		var y = startY;
		var w = endX - startX;
		var h = endY - startY;
		ctx.beginPath();
		ctx.strokeRect(x,y,w,h);
		ctx.closePath();
	}
	this.strokeRect = function(startX,startY,endX,endY) {
		var x = startX;
		var y = startY;
		var w = endX - startX;
		var h = endY - startY;
		ctx.fillRect(x,y,w,h);
	}
	this.ellipse = function(startX,startY,endX,endY) {
		var a = Math.abs((endX-startX)/2);
		var b = Math.abs(endY-startY);
		var x;
		if (endX > startX) {
			x = startX + a;
		} else {
			x = startX - a;
		}
		var y = startY;
		var k = 0.5522848;
		ox = a * k;
		oy = b * k;
		ctx.beginPath();
		ctx.moveTo(x-a,y);
		ctx.bezierCurveTo(x-a,y-oy,x-ox,y-b,x,y-b);
		ctx.bezierCurveTo(x+ox,y-b,x+a,y-oy,x+a,y);
		ctx.bezierCurveTo(x+a,y+oy,x+ox,y+b,x,y+b);
		ctx.bezierCurveTo(x-ox,y+b,x-a,y+oy,x-a,y);
		ctx.stroke();
		ctx.closePath();
	}
	this.circle = function(x,y,r) {
		ctx.beginPath();
		ctx.arc(x,y,r,Math.PI*2,0,false);
		ctx.stroke();
		ctx.closePath();
	}
	this.roundRect = function(startX,startY,endX,endY) {
		var r = 10;
		var x = startX;
		var y = startY;
		var w = endX-startX;
		var h = endY-startY;
		ctx.beginPath();
		ctx.moveTo(x+r,y);
		ctx.lineTo(x+w-r,y);
		ctx.quadraticCurveTo(x+w,y,x+w,y+r);
		ctx.lineTo(x+w,y+h-r);
		ctx.quadraticCurveTo(x+w,y+h,x+w-r,y+h);
		ctx.lineTo(x+r,y+h);
		ctx.quadraticCurveTo(x,y+h,x,y+h-r);
		ctx.lineTo(x,y+r);
		ctx.quadraticCurveTo(x,y,x+r,y);
		ctx.closePath();
		ctx.stroke();
	}
	this.triangle = function(startX,startY,endX,endY) {
		var x = startX;
		var y = endY;
		ctx.beginPath();
		ctx.moveTo(startX,startY);
		ctx.lineTo(endX,endY);
		ctx.lineTo(x,y);
		ctx.lineTo(startX,startY);
		ctx.stroke();
		ctx.closePath();
	}
	this.eraser = function(x,y,t,r) {
		ctx.clearRect(x,y,r,r);
	}
	this.star = function(x1,y1,x2,y2) {
		var r = (x2 - x1)/2;
		ctx.beginPath();
		var dit = Math.PI*4/5;
		var sin = Math.sin(0)*r + y1;
		var cos = Math.cos(0)*r + x1;
		ctx.moveTo(cos,sin);
		var p = new Array();
		var i;
		for ( i = 0;i<5;i++) {
			var tmpDit = dit*i;
			sin = Math.sin(tmpDit)*r+y1;
			cos = Math.cos(tmpDit)*r+x1;
			p.push([cos,sin]);
		}
		ctx.moveTo(p[0][0],p[0][1]);
		for (i = 0;i<5;i++){
			ctx.lineTo(p[i][0],p[i][1]);
		}
		ctx.lineTo(p[0][0],p[0][1]);
		ctx.stroke();
		ctx.closePath();
	}
	//画虚线框
	this.dashedRect = function(x1,y1,x2,y2,w,p) {
		if (typeof p == "undefined"){
			p = 5;
		}
		this.dashedLine(x1,y1,x2,y1,p);
		this.dashedLine(x2,y1,x2,y2,p);
		this.dashedLine(x2,y2,x1,y2,p);
		this.dashedLine(x1,y2,x1,y1,p);
	}
	//画虚线段
	this.dashedLine = function(fromX,fromY,toX,toY,pattern) {
		if (typeof pattern == "undefined"){
			pattern = 5;
		}
		// calculate the delta x and delta y  
    	var dx = (toX - fromX);  
    	var dy = (toY - fromY);  
    	var distance = Math.floor(Math.sqrt(dx*dx + dy*dy));  
    	var dashlineInteveral = (pattern <= 0) ? distance : (distance/pattern);  
    	var deltay = (dy/distance) * pattern;  
    	var deltax = (dx/distance) * pattern;  
      	ctx.lineWidth = 1;
		//ctx.strokeStyle = "rgb(128,128,255)";
		ctx.strokeStyle = "#000000";
    	// draw dash line  
    	ctx.beginPath();  
    	for(var dl=0; dl<dashlineInteveral; dl++) {  
        	if(dl%2) {  
          		ctx.lineTo(fromX + dl*deltax, fromY + dl*deltay);  
        	} else {                      
            	ctx.moveTo(fromX + dl*deltax, fromY + dl*deltay);                    
        	}                 
    	}  
    	ctx.stroke();  
		ctx.closePath();
	}
	//剪切
	this.clip = function(x,y,w,h) {
		//ctx.globalAlpha = 0.0;
		
	}
	//绘制文字功能
	this.drawText = function(content,x,y) {
		//ctx.font = tool.getFontStyle();
		ctx.textAlign = "lfet";
		ctx.textBaseline = "top";
		ctx.font = "bold 30px Arial";
		ctx.strokeStyle = "#000000";
		ctx.beginPath();
		ctx.strokeText("Name",x,y);
		ctx.fillText("Name",x,y);
		ctx.closePath();
		console.log("Draw Text at ",content,x,y);
	}
	
	//颜色拾取器
	this.colorPicker = function(x,y) {
		var imageData = ctx.getImageData(x,y,1,1);
		var pixel = imageData.data;
		console.log(pixel);
		var pixelColor = "rgba("+pixel[0]+", "+pixel[1]+", "+pixel[2]+", "+pixel[3]+")";
		return pixelColor;
	}
	this.fill = function(x,y,color) {
		
	}
 
}
